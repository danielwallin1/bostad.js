### Bostad.js chrome extension script

A small little chrome extension helper script that adds a column for queue position on the search appartment view on bostad stockholm. This way you don't have to open up the link for the appartment to find out.

### Instructions
-- Download this repo

-- In chrome go to "chrome://extensions/"

-- Check developer mode and load this folder 

-- Go to bostad.stockholm to check it out


