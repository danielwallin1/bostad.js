(function($){

	var BSF = {

		init : function () {
			this.setVars();
			this.addColumn();
			this.setEvents();
		}, 

		setVars : function () {
			this.$wrapper = $("#DataTables_Table_0_wrapper"); 
			this.$table = $("table", this.wrapper); 
			this.$th = $("thead", this.table); 
			this.$tbody = $("tbody", this.table); 
			this.$tr = $("tr", this.tbody); 

			this.$loader = $("#loading"); 

			this.$table.css("table-layout", "fixed");
		}, 		

		setEvents : function () { 
			var base = this; 
			$(".fetch").on("click", function (e) {										
				e.stopPropagation(); 
				base.$currentrow = $(e.target).parent();	
				var url = base.$currentrow.find("a").first().attr("href"); 
				base.fetch(url);				
			}); 
		}, 

		addColumn : function () {
			var base = this; 
			this.$th.find("tr").append("<th class='sorting_asc'>Placering</th>"); 		
			$.each(this.$tr, function () {
				$(this).append("<td class='fetch'>Hämta placering</td>"); 
			}); 
			base.$th.find("td").remove();
		}, 

		update : function () {
			var base = this;
			base.$currentrow.find(".fetch").html(base.pos);
		}, 

		fetch : function (url) {
			var base = this; 
			this.$loader.show();
			$.get(url, function( data ) {
			  	base.pos = $(data).find(".data.interest .greenLbl").text();
			  	base.$loader.hide();
			  	base.update(); 
			});	
		}  	

		
	}; 	

	BSF.init();
} )( jQuery );


